<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin extends CI_Model
{
    //fungsi cek session
    function logged_id()
    {
        return $this->session->userdata('user_id');
    }

    function tampilData()
    {
    $query=$this->db->get('mahasiswa');
        if ($query->num_rows()>0)
        {
        return $query->result();
        }
        else
        {
        return array();
        }
    }

    //fungsi check login
    function check_login($table, $field1, $field2)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($field1);
        $this->db->where($field2);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result();
        }
    }

    function tambah($data)
    {
    $tambah=$this->db->insert('mahasiswa',$data);
    return $tambah;
    }

    function per_nim($id)
    {
    $this->db->where('nim',$id);
    $query=$this->db->get('mahasiswa');
    return $query->result();
    }

    public function insert($data){
        $this->db->insert('ccf', $data);
    }

    public function getData(){
        //$this->db->where('is_approve', '0');
        $query = $this->db->get('ccf');

        return $query->result();
    }

    public function getDataByNoCCF($no_ccf){
        $this->db->where('no_ccf', $no_ccf);
        $query = $this->db->get('ccf');

        return $query->result_array();
    }

    public function update_approve($data){
       
        $query = $this->db->query('UPDATE ccf SET is_approve="'.$data['is_approve'].'" WHERE no_ccf="'.$data['no_ccf'].'"');

        return $query;
    }
}