<?php
$level = $this->session->userdata('level');
?>

<div class="col-md-3">
            <div class="list-group">
              <a href="#" class="list-group-item active" style="text-align: center;background-color: black;border-color: black">
                ADMINISTRATOR
              </a>
            <a href="http://localhost:8080/ccf/dashboard/dashboard2" class="list-group-item"><i class="fa fa-dashboard"></i> Dashboard</a>

              <?php if($level=='user') { ?>
              <a href="http://localhost:8080/ccf/dashboard/ccf" class="list-group-item"><i class="fa fa-book"></i> CCF</a>
              <?php } ?>

               <?php if($level=='admin') { ?>
              <a href="http://localhost:8080/ccf/dashboard/data_ccf" class="list-group-item"><i class="fa fa-book"></i> Data CCF</a>
               <?php } ?>
               
              <a href="http://localhost:8080/ccf/dashboard/logout" class="list-group-item"><i class="fa fa-sign-out"></i> Logout</a>
            </div>
        </div>