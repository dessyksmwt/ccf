<!DOCTYPE html>
<html>
<head>
    <title> Dashboard - Login CodeIgniter & Bootstrap</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/DataTables/media/css/jquery.dataTables.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/sweetalert/sweetalert.css">

    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">BAF</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="navbar-form navbar-right">
                <a href="<?php echo base_url() ?>dashboard/logout" type="submit" class="btn btn-success"><i class="fa fa-sign-out"></i> Logout</a>
            </div>
      </div>
    </nav>
<div class="container" style="margin-top: 80px">
    <div class="row">
       <!-- Menu -->
        <?php require_once(APPPATH."views/menu.php");?>
        <!-- Menu -->
        <div class="col-md-9">
            <div class="panel panel-default">


            
<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">CCF</a></li>
  <li><a data-toggle="tab" href="#menu1">Rollback</a></li>
</ul>

 <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-dashboard"></i> Change Configuration Form</h3>
              </div>
                <div class="panel-body">


<div class="tab-content">
  <div id="home" class="tab-pane fade in active">

<form action="<?=base_url('dashboard/insert');?>" method="post" enctype="multipart/form-data">
     <center>
<table border="2" class="table text-center" >
 <div >
  <tr >
    <td rowspan="2"><img src="https://www.baf.id/Berita-dan-Acara/mobile/baf.png" height="50" width="50" /></td>
    <td rowspan="2" style="vertical-align : middle;text-align:center;"><b>CHANGE CONFIGURATION FORM</b></td>
    <td><b>IT-0024</b></td>
  </tr>

  <tr class="center">
    <td><b>IT HQ</b></td>
  </tr>
</div>
</table>

      <!-- Table CCF -->
      <!-- TABEL NO 1 -->
      
<table border="3" style="width: 100%">
  <tr>
    <td>
 <table border="0" rules="0" class="table text-right">
  <div>
  <tr>
    <td>  
    <label>No .
     <input type="text" name="no" required></input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL CHANGE CATEGORY -->
    <table border="0" class="table" rules="none">
  <div>
  <tr>
    <td>
    <label>Change Category :
      <br>
      <input type="radio" value="Permanent" name="change-category"> Permanent </input> &nbsp;
      <input type="radio" value="Temporary" name="change-category"> Temporary / Trial</input> &nbsp;
  </label>
    </label>
  </td>
    <td class="text-right">
    <label>Due Date :
     <input type="date" name="due-date"></input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL TESTING -->
    <table border="0" class="table" rules="none">
  <div>
  <tr>
    <td>
    <label>Testing Required :
      <input type="radio" value="Yes" name="testing_required"> Yes </input> &nbsp;
      <input type="radio" value="No" name="testing_required"> No</input> &nbsp;
  </label>
    </label>
  </td>
    <td class="text-right">
    <label>Tested On :
      <input type="checkbox" value="Production" name="tested_on_production"> Production </input> &nbsp;
      <input type="checkbox" value="Development" name="tested_on_development"> Development</input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL NOTE  -->
<table border="0" rules="none" class="table">
  <div>
  <tr>
    <td>  
    <label>Note :
      <br>
     <textarea class="form-control" name="note" rows="5"  cols="200"></textarea>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL DATE OF REQUEST -->
<table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
    <label>Date Of Request :
     <input type="date" name="date-of-request"></input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL AFFECTED -->
<table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
    <label>Affected Sector :
     <input type="text" name="affected-sector" style="width: 500px" ></input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL PURPOSE -->
<table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
      <label> Purpose Of Modification :
      </label>
    </td>
  </tr>
    <tr>
    <td>
       <textarea class="form-control" name="purpose-of-modification" ></textarea>
     </td>
   </tr>
</div>
</table>

<!-- TABEL EXECUTION -->
<table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
    <label>Execution Date :
     <input type="date" name="execution-date" ></input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL EQUIPMENTS --> 
      <label> Equipments Affected :
      </label>


    <table border="0" rules="none" class="table text-left" style="width: 100%">
      <tr>
  <td width="100px">
    <input type="checkbox" value="Permanent" name="checkbox_equipmenct_affected" id="network-devices"> Network Devices </input> &nbsp;
    <textarea class="form-control" rows="4" name="network-devices" id="network-devices-textarea" disabled></textarea>
  </td>
  <td width="100px">
  <input type="checkbox" value="Permanent" name="checkbox_equipmenct_affected" id="servers"> Serves </input> &nbsp;
  <textarea class="form-control" rows="4" name="servers" id="servers-textarea" disabled></textarea>
  </td>
  <td width="100px"><input type="checkbox" value="Permanent" name="checkbox_equipmenct_affected" id="others"> Others </input> &nbsp;
  <textarea class="form-control" rows="4" name="others" id="others-textarea" disabled></textarea>
  </td>
</tr>
</table>

<!-- TABEL CHANGE -->
    <table border="0" rules="none" class="table text-left" style="width: 100%">
<tr>
  <td width="100px">
   <label> Detailed Change Description :
      </label>
    <textarea class="form-control" rows="10" name="detailed-change-des" ></textarea>
  </td>
  <td width="100px">
  <label> New Configuration :
      </label>
  <textarea class="form-control" rows="10" name="new-configuration" ></textarea>
  </td>
</tr>
</table>

<!-- TABEL DETAILED -->
<table border="0" rules="none" class="table text-left">
  <tr>
    <td>  
      <label> Detailed Change Impact :
      </label>
    </td>
  </tr>
  </table>
    <table border="0" rules="none" class="table text-left" style="width: 100%">
<tr>
  <td width="100px">
    <label> Business Impact :
      </label>
    <textarea class="form-control" rows="4" name="business-impact" ></textarea>
  </td>
  <td width="100px">
     <label> Security Impact :
      </label>
  <textarea class="form-control" rows="4" name="security-impact"></textarea>
  </td>
</tr>
</table>

<!-- TABEL ATTACHMENT -->
<table border="0" rules="none"  class="table text-left">
  <div>
  <tr>
    <td>  
      <label> Attachment :
      </label>
    </td>
  </tr>
    <tr>
    <td>
      <?php echo form_open_multipart('upload/do_upload');?>
       <input type="File" class="form-control" name="attachment" ></input>
     </td>
   </tr>
</div>
</table>


</td>
</tr>

</div>
</div>

 </table>
    <!-- Table CCF -->

  </div>
  <div id="menu1" class="tab-pane fade">

    <!-- tabel rollback -->
<!-- TABEL NO 1 -->

    <center>
<table border="2" class="table text-center" >
 <div >
  <tr >
    <td rowspan="2"><img src="https://www.baf.id/Berita-dan-Acara/mobile/baf.png" height="50" width="50" /></td>
    <td rowspan="2" style="vertical-align : middle;text-align:center;"><b>CHANGE CONFIGURATION FORM</b></td>
    <td><b>IT-0024</b></td>
  </tr>

  <tr class="center">
    <td><b>IT HQ</b></td>
  </tr>
</div>
</table>


<table border="3" style="width: 100%">
  <tr>
    <td>
 <table border="0 rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
    <label>Bussan Auto Finance
    </label>
  </td>
  </tr>
</div>
</table>

<table border="0" rules="none" style="width: 100%">
  <tr>
    <td>
 <table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
    <label>Date Request :
       <input type="date" name="date-request"></input>
    </label>
  </td>
  </tr>
  <tr>
    <td>  
    <label>Department :
       <input type="text" name="department"></input>
    </label>
  </td>
  </tr>
  <tr>
    <td>  
    <label>Purpose :<br>
       <textarea name="purpose"></textarea> 
    </label>
  </td>
  </tr>
</div>
</table>

<table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
    <label>Equipments Affected :
    </label>
  </td>
  </tr>
</div>
</table>

 <table border="0" rules="none" class="table" style="width: 100%">
<tr>
  <td >
   <input type="checkbox" size="5" name="checkbox_equipment_affected_rollback" id="type1">
  </td>
  <td>
    <label>Type :
    </label>
   <input type="text" size="40" name="equipment_affected_rollback_type[]" id="type1_input" disabled></input>
   <label>IP Address :
    </label>
    <input type="text" size="10" name="equipment_affected_rollback_ip[]" id="ip1_input" disabled></input>
  </td>
</tr>

<tr>
  <td >
   <input type="checkbox" size="5" name="checkbox_equipment_affected_rollback" id="type2">
  </td>
  <td>
  <label>Type :
    </label>
   <input type="text" size="40" name="equipment_affected_rollback_type[]" id="type2_input" disabled></input>
   <label>IP Address :
    </label>
    <input type="text" size="10" name="equipment_affected_rollback_ip[]" id="ip2_input" disabled></input>
  </td>
</tr>

<tr>
  <td >
   <input type="checkbox" size="5" name="checkbox_equipment_affected_rollback" id="type3">
  </td>
  <td>
  <label>Type :
    </label>
   <input type="text" size="40" name="equipment_affected_rollback_type[]" id="type3_input" disabled></input>
   <label>IP Address :
    </label>
    <input type="text" size="10" name="equipment_affected_rollback_ip[]" id="ip3_input" disabled></input>
  </td>
</tr>

<tr>
  <td >
   <input type="checkbox" size="5" name="checkbox_equipment_affected_rollback" id="type4">
  </td>
  <td>
  <label>Type :
    </label>
   <input type="text" size="40" name="equipment_affected_rollback_type[]" id="type4_input" disabled></input>
   <label>IP Address :
    </label>
    <input type="text" size="10" name="equipment_affected_rollback_ip[]" id="ip4_input" disabled></input>
  </td>
</tr>
</table>

<table border="0" rules="none"  class="table text-left">
  <div>
  <tr>
    <td>  
      <label> Descriptions :
      </label>
    </td>
  </tr>
  <tr>
    <td>  
      <label> Configuration before Rollback :
      </label>
    </td>
  </tr>
    <tr>
    <td>
       <textarea class="form-control" rows="10" name="configuration-before-rollback"></textarea>
     </td>
   </tr>

   <tr>
    <td>  
      <label> Configuration Plan of Rollback :
      </label>
    </td>
  </tr>
    <tr>
    <td>
       <textarea class="form-control" rows="10" name="configuration-plan-rollback"></textarea>
     </td>
   </tr>
</div>

</table>

<div class="pull-right padding-right btn-space  margin-right  padding-right" >
<button type="submit" class="btn btn-primary">Save</button>
</center>
 
</div>

</td>
</tr>

</form>
 <!-- tabel rollback -->

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
<script src="<?=base_url();?>assets/sweetalert/sweetalert.min.js"></script>
<script src="<?=base_url();?>assets/alert/js/qunit-1.18.0.js"></script>

<script type="text/javascript">
  //CCF
  $('#network-devices').change(function(){

    isChecked('network-devices', 'network-devices-textarea');

  });

  $('#servers').change(function(){

  isChecked('servers', 'servers-textarea');

  });

  $('#others').change(function(){

  isChecked('others', 'others-textarea');

  });
//CCF

// ROLLBACK
  $('#type1').change(function(){

  isCheckedRollback('type1', 'type1_input', 'ip1_input');

  });

  $('#type2').change(function(){

  isCheckedRollback('type2', 'type2_input', 'ip2_input');

  });

  $('#type3').change(function(){

  isCheckedRollback('type3', 'type3_input', 'ip3_input');

  });

  $('#type4').change(function(){

  isCheckedRollback('type4', 'type4_input', 'ip4_input');

  });
// ROLLBACK
  function isChecked(id_checkbox, id_textarea){

    var check = $('#'+id_checkbox).is(':checked');

    if(check===false){
      $('#'+id_textarea).prop('disabled', true).val('');
    } else {
      $('#'+id_textarea).prop('disabled', false).val('');
    }
  }

  function isCheckedRollback(id_checkbox, id_input_type, id_input_ip){

  var check = $('#'+id_checkbox).is(':checked');

  if(check===false){
    $('#'+id_input_type).prop('disabled', true).val('');
    $('#'+id_input_ip).prop('disabled', true).val('');
  } else {
    $('#'+id_input_type).prop('disabled', false).val('');
    $('#'+id_input_ip).prop('disabled', false).val('');
  }
  }
</script>

<!-- <script type="text/javascript">
var url = '<?=base_url('Dashboard/tambah');?>';
   $('#ini-form').on('submit',function(e) {
    swal({
    title: "Simpan Data",
    text: "Apakah anda ingin menyimpan data ini ?",
    confirmButtonText:"Yakin",
    confirmButtonColor: "#002855",
    cancelButtonText:"Tidak",
    showCancelButton: true,
    closeOnConfirm: false,
    imageUrl: '<?=base_url('assets/images/imagessure.png');?>',
    showLoaderOnConfirm: true
    }, function () {
    $.ajax({
    url:url,
    data:$('#ini-form').serialize(),
    dataType:'text',
    type:'POST',
    success:function(e){    
if (e !== "gagal") {
  swal({
    title: "Success",
    confirmButtonColor: "#002855",
    text: "Data berhasil disimpan !.",
    imageUrl: '<?=base_url('assets/images/emotgood1.png');?>',
    },function(){
      window.location= '<?=base_url('dashboard/tambahdata');?>';
    });
    }
    else{
  swal({
    title: "Failed",
    confirmButtonColor: "#002855",
    text: "Data tidak berhasil disimpan !.",
    imageUrl: '<?=base_url('assets/images/emotsad.png');?>',
    });
    } 
    },
  error:function(xhr, ajaxOptions, thrownError){
  swal({
    title: "Failed",
    confirmButtonColor: "#002855",
    text: "Data tidak berhasil disimpan !.",
    imageUrl: '<?=base_url('assets/images/emotsad.png');?>',
  });
  }
  });
  return false;
  });
  e.preventDefault(); 
  });
</script> -->
 
</body>
</html>
