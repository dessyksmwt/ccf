<!DOCTYPE html>
<html>
<head>
    <title> Dashboard - Login CodeIgniter & Bootstrap</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/DataTables/media/css/jquery.dataTables.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/sweetalert/sweetalert.css">

    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">BAF</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="navbar-form navbar-right">
                <a href="<?php echo base_url() ?>dashboard/logout" type="submit" class="btn btn-success"><i class="fa fa-sign-out"></i> Logout</a>
            </div>
      </div>
    </nav>
<div class="container" style="margin-top: 80px">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group">
              <a href="#" class="list-group-item active" style="text-align: center;background-color: black;border-color: black">
                ADMINISTRATOR
              </a>
             <a href="<?php echo base_url() ?>dashboard/dashboard2" class="list-group-item"><i class="fa fa-dashboard"></i> Dashboard</a>
              <a href="<?php echo base_url() ?>dashboard/tambahdata" class="list-group-item"><i class="fa fa-book"></i> CCF</a>
              <a href="<?php echo base_url() ?>dashboard/logout" class="list-group-item"><i class="fa fa-sign-out"></i> Logout</a>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">

<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#ccf">CCF</a></li>
  <li><a data-toggle="tab" href="#roll">Rollback Plan</a></li>
</ul>



              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-dashboard"></i> Change Configuration Form</h3>
              </div>
              <div class="panel-body">

<div class="tab-content">
  <div id="roll" class="tab-pane fade in active">
   <center>
<table border="2" class="table text-center" >
 <div >
  <tr >
    <td rowspan="2"><img src="https://www.baf.id/Berita-dan-Acara/mobile/baf.png" height="50" width="50" /></td>
    <td rowspan="2" style="vertical-align : middle;text-align:center;"><b>CHANGE CONFIGURATION FORM</b></td>
    <td><b>IT-0024</b></td>
  </tr>

  <tr class="center">
    <td><b>IT HQ</b></td>
  </tr>
</div>
</table>

<!-- tabel baru -->
<!-- TABEL NO 1 -->
<table border="1" style="width: 100%">
  <tr>
    <td>
 <table border="0" rules="0" class="table text-right">
  <div>
  <tr>
    <td>  
    <label>No .
     <input type="text"></input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL CHANGE CATEGORY -->
    <table border="0" class="table" rules="none">
  <div>
  <tr>
    <td>
    <label>Change Category :
      <br> 
      <input type="checkbox" value="Permanent" name="checkbox"> Permanent </input> &nbsp;
      <input type="checkbox" value="Temporary" name="checkbox"> Temporary / Trial</input>
  </label>
    </label>
  </td>
    <td class="text-right">
    <label>Due Date :
     <input type="date" ></input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL TESTING -->
    <table border="0" class="table" rules="none">
  <div>
  <tr>
    <td>
    <label>Testing Required :
      <input type="checkbox" value="Permanent" name="checkbox"> Yes </input> &nbsp;
      <input type="checkbox" value="Temporary" name="checkbox"> No</input>
  </label>
    </label>
  </td>
    <td class="text-right">
    <label>Tested On :
      <input type="checkbox" value="Permanent" name="checkbox"> Production </input> &nbsp;
      <input type="checkbox" value="Temporary" name="checkbox"> Development</input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL NOTE  -->
<table border="0" rules="none" class="table">
  <div>
  <tr>
    <td>  
    <label>Note :
      <br>
     <textarea class="form-control" rows="5"  cols="200"></textarea>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL DATE OF REQUEST -->
<table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
    <label>Date Of Request :
     <input type="date" ></input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL AFFECTED -->
<table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
    <label>Affected Sector :
     <input type="text" style="width: 500px" ></input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL PURPOSE -->
<table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
      <label> Purpose Of Modification :
      </label>
    </td>
  </tr>
    <tr>
    <td>
       <textarea class="form-control" ></textarea>
     </td>
   </tr>
</div>
</table>



</td>
</tr>

 </table>
</center>
</div>
</div>
              </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
<script src="<?=base_url();?>assets/sweetalert/sweetalert.min.js"></script>
<script src="<?=base_url();?>assets/alert/js/qunit-1.18.0.js"></script>

<script type="text/javascript">
var url = '<?=base_url('Dashboard/tambah');?>';
   $('#ini-form').on('submit',function(e) {
    swal({
    title: "Simpan Data",
    text: "Apakah anda ingin menyimpan data ini ?",
    confirmButtonText:"Yakin",
    confirmButtonColor: "#002855",
    cancelButtonText:"Tidak",
    showCancelButton: true,
    closeOnConfirm: false,
    imageUrl: '<?=base_url('assets/images/imagessure.png');?>',
    showLoaderOnConfirm: true
    }, function () {
    $.ajax({
    url:url,
    data:$('#ini-form').serialize(),
    dataType:'text',
    type:'POST',
    success:function(e){    
if (e !== "gagal") {
  swal({
    title: "Success",
    confirmButtonColor: "#002855",
    text: "Data berhasil disimpan !.",
    imageUrl: '<?=base_url('assets/images/emotgood1.png');?>',
    },function(){
      window.location= '<?=base_url('dashboard/tambahdata');?>';
    });
    }
    else{
  swal({
    title: "Failed",
    confirmButtonColor: "#002855",
    text: "Data tidak berhasil disimpan !.",
    imageUrl: '<?=base_url('assets/images/emotsad.png');?>',
    });
    } 
    },
  error:function(xhr, ajaxOptions, thrownError){
  swal({
    title: "Failed",
    confirmButtonColor: "#002855",
    text: "Data tidak berhasil disimpan !.",
    imageUrl: '<?=base_url('assets/images/emotsad.png');?>',
  });
  }
  });
  return false;
  });
  e.preventDefault(); 
  });
</script>
 
</body>
</html>