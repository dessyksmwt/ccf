<!DOCTYPE html>
<html>
<head>
    <title> Dashboard - Login CodeIgniter & Bootstrap</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" href="<?=base_url();?>assets/sweetalert/sweetalert.css">

    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">BAF</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="navbar-form navbar-right">
                <a href="<?php echo base_url() ?>dashboard/logout" type="submit" class="btn btn-success"><i class="fa fa-sign-out"></i> Logout</a>
            </div>
      </div>
    </nav>
<div class="container" style="margin-top: 80px">
    <div class="row">

        
        <!-- Menu -->
        <?php require_once(APPPATH."views/menu.php");?>
        <!-- Menu -->


        <div class="col-md-9">
            <div class="panel panel-default">


            


 <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-dashboard"></i> Change Configuration Form</h3>
              </div>
                <div class="panel-body">


<div class="tab-content">
  <div id="home" class="tab-pane fade in active">

<!-- HEADER -->
     <center>
<table border="2" class="table text-center" >
 <div >
  <tr >
    <td rowspan="2"><img src="https://www.baf.id/Berita-dan-Acara/mobile/baf.png" height="50" width="50" /></td>
    <td rowspan="2" style="vertical-align : middle;text-align:center;"><b>CHANGE CONFIGURATION FORM</b></td>
    <td><b>IT-0024</b></td>
  </tr>

  <tr class="center">
    <td><b>IT HQ</b></td>
  </tr>
</div>
</table>
</center>
<!-- HEADER -->

<!-- TABEL DATA CCF -->
	<table class="table table-striped table-bordered data">
			<thead>
				<tr>			
					<th>No</th>
					<th>Change Category</th>
					<th>Due Date</th>
					<th>Testing Required</th>
					<th>Tested On</th>
					<th>Note</th>
          <th>Approve</th>
					<th>Action</th>
				</tr>
			</thead>
				<tbody>
				
				<?php foreach ($ccf as $row):
          if ($row->is_approve==1) {
            $row->is_approve='Approve';
          } elseif ( $row->is_approve==2) {
             $row->is_approve='Reject';
          } elseif ( $row->is_approve==0) {
             $row->is_approve='Wait';
          }
          ?>

				<tr>				
					<td><?php echo $row->no_ccf;?></td>
					<td><?php echo $row->change_category;?></td>
					<td><?php echo $row->due_date;?></td>
					<td><?php echo $row->testing_required;?></td>
					<td><?php echo $row->tested_on;?></td>
					<td><?php echo $row->note;?></td>
          <td><?php echo $row->is_approve;?></td>
					<td><a class="btn btn-success" href = '<?php base_url();?>tambahdata?no_ccf=<?php echo $row->no_ccf;?>'>Lihat</a></td>
				</tr>
				<?php endforeach;?>
				</tbody>
				
		</table>



<!-- TABEL DATA CCF -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
<script src="<?=base_url();?>assets/sweetalert/sweetalert.min.js"></script>
<script src="<?=base_url();?>assets/alert/js/qunit-1.18.0.js"></script>
 
 <script type="text/javascript">
  $(document).ready(function(){
    $('.data').DataTable({
      "aLengthMenu": [[5, 10, 15, 20], [5, 10, 15, 20]],
    }); 
  });
</script>
</body>
</html>
