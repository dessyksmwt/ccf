<!DOCTYPE html>
<html>
<head>
    <title> Dashboard - Login CodeIgniter & Bootstrap</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/DataTables/media/css/jquery.dataTables.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/sweetalert/sweetalert.css">

    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">BAF</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="navbar-form navbar-right">
                <a href="<?php echo base_url() ?>dashboard/logout" type="submit" class="btn btn-success"><i class="fa fa-sign-out"></i> Logout</a>
            </div>
      </div>
    </nav>
<div class="container" style="margin-top: 80px">
    <div class="row">
         <!-- Menu -->
        <?php require_once(APPPATH."views/menu.php");?>
        <!-- Menu -->
        <div class="col-md-9">
            <div class="panel panel-default">


            
<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">CCF</a></li>
  <li><a data-toggle="tab" href="#menu1">Rollback</a></li>
</ul>

 <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-dashboard"></i> Change Configuration Form</h3>
              </div>
                <div class="panel-body">


<div class="tab-content">
  <div id="home" class="tab-pane fade in active">

  <form method="post" action="<?=base_url('dashboard/approve');?>">
     <center>
<table border="2" class="table text-center" >
 <div >
  <tr >
    <td rowspan="2"><img src="https://www.baf.id/Berita-dan-Acara/mobile/baf.png" height="50" width="50" /></td>
    <td rowspan="2" style="vertical-align : middle;text-align:center;"><b>CHANGE CONFIGURATION FORM</b></td>
    <td><b>IT-0024</b></td>
  </tr>

  <tr class="center">
    <td><b>IT HQ</b></td>
  </tr>
</div>
</table>

      <!-- Table CCF -->
      <!-- TABEL NO 1 -->
<table border="3" style="width: 100%">
  <tr>
    <td>
 <table border="0" rules="0" class="table text-right">
  <div>
  <tr>
    <td>  
    <label>No .
     <input type="text" readonly="" value="<?=$no_ccf;?>" name="no_ccf"/>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL CHANGE CATEGORY -->
    <table border="0" class="table" rules="none">
  <div>
  <tr>
    <td>
    <label>Change Category :
      <br> 

      <?php if($change_category=='Permanent'){
        $permanent = 'checked';
        $temporary = '';
      } else {
        $permanent = '';
        $temporary = 'checked';
      }
      ?>
      <input type="radio" value="Permanent" name="permanent" <?=$permanent;?>> Permanent </input> &nbsp;

      <input type="radio" value="Temporary" name="temporary" <?=$temporary;?>> Temporary / Trial</input>
  </label>
    </label>
  </td>
    <td class="text-right">
    <label>Due Date :
     <input type="date" value="<?=$due_date;?>"></input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL TESTING -->
    <table border="0" class="table" rules="none">
  <div>
  <tr>
    <td>
    <label>Testing Required :
       <?php if($testing_required=='Yes'){
        $yes = 'checked';
        $no = '';
      } else {
        $yes = '';
        $no = 'checked';
      }
      ?>
      <input type="radio" value="Yes" name="yes" <?=$yes;?>> Yes </input> &nbsp;
      <input type="radio" value="No" name="no" <?=$no;?>> No</input>
  </label>
    </label>
  </td>
    <td class="text-right">
    <label>Tested On :
      <input type="checkbox" value="Permanent" name="checkbox" <?=$production;?>> Production </input> &nbsp;
      <input type="checkbox" value="Temporary" name="checkbox" <?=$development;?>> Development</input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL NOTE  -->
<table border="0" rules="none" class="table">
  <div>
  <tr>
    <td>  
    <label>Note :
      <br>
     <textarea class="form-control" rows="5"  cols="200"><?=$note;?></textarea>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL DATE OF REQUEST -->
<table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
    <label>Date Of Request :
     <input type="date" value="<?=$date_of_request;?>"></input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL AFFECTED -->
<table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
    <label>Affected Sector :
     <input type="text" style="width: 500px" value="<?=$affected_sector;?>"></input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL PURPOSE -->
<table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
      <label> Purpose Of Modification :
      </label>
    </td>
  </tr>
    <tr>
    <td>
       <textarea class="form-control" ><?=$purpose_of_modification;?></textarea>
     </td>
   </tr>
</div>
</table>

<!-- TABEL EXECUTION -->
<table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
    <label>Execution Date :
     <input type="date" value="<?=$execution_date;?>" ></input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL EQUIPMENTS --> 
      <label> Equipments Affected :
      </label>


    <table border="0" rules="none" class="table text-left" style="width: 100%">
      <tr>
  <td width="100px">

    <?php if($network_devices == '') { ?>
    <input type="checkbox" value="Permanent" name="checkbox" > Network Devices </input> &nbsp;
    <?php } else { ?>
    <input type="checkbox" value="Permanent" name="checkbox" checked> Network Devices </input> &nbsp;
    <?php } ?>

    <textarea class="form-control" rows="4" ><?=$network_devices;?></textarea>
  
  </td>
  <td width="100px">
    <?php if($servers == '') { ?>
    <input type="checkbox" value="Permanent" name="checkbox" > Servers </input> &nbsp;
    <?php } else { ?>
    <input type="checkbox" value="Permanent" name="checkbox" checked> Servers </input> &nbsp;
    <?php } ?>

  <textarea class="form-control" rows="4"><?=$servers;?></textarea>
  </td>
  <td width="100px">
    <?php if($others == '') { ?>
    <input type="checkbox" value="Permanent" name="checkbox" > Others </input> &nbsp;
    <?php } else { ?>
    <input type="checkbox" value="Permanent" name="checkbox" checked> Others </input> &nbsp;
    <?php } ?>

  <textarea class="form-control" rows="4"> <?=$others;?></textarea>
  </td>
</tr>
</table>

<!-- TABEL CHANGE -->
    <table border="0" rules="none" class="table text-left" style="width: 100%">
<tr>
  <td width="100px">
   <label> Detailed Change Description :
      </label>
    <textarea class="form-control" rows="10" ><?=$detailed_change_description;?></textarea>
  </td>
  <td width="100px">
  <label> New Configuration :
      </label>
  <textarea class="form-control" rows="10" ><?=$new_configuration;?></textarea>
  </td>
</tr>
</table>

<!-- TABEL DETAILED -->
<table border="0" rules="none" class="table text-left">
  <tr>
    <td>  
      <label> Detailed Change Impact :
      </label>
    </td>
  </tr>
  </table>
    <table border="0" rules="none" class="table text-left" style="width: 100%">
<tr>
  <td width="100px">
    <label> Business Impact :
      </label>
    <textarea class="form-control" rows="4" ><?=$business_impact;?></textarea>
  </td>
  <td width="100px">
     <label> Security Impact :
      </label>
  <textarea class="form-control" rows="4" ><?=$security_impact;?></textarea>
  </td>
</tr>
</table>

<!-- TABEL ATTACHMENT -->
<table border="0" rules="none"  class="table text-left">
  <div>
  <tr>
    <td>  
      <label> Attachment :
      </label>
    </td>
  </tr>
    <tr>
    <td>
       <textarea class="form-control" ><?=$attachment;?></textarea>
     </td>
   </tr>
</div>
</table>


</td>
</tr>

</div>
</div>

 </table>
    <!-- Table CCF -->

  </div>
  <div id="menu1" class="tab-pane fade">

    <!-- tabel rollback -->
<!-- TABEL NO 1 -->

      

    <center>
<table border="2" class="table text-center" >
 <div >
  <tr >
    <td rowspan="2"><img src="https://www.baf.id/Berita-dan-Acara/mobile/baf.png" height="50" width="50" /></td>
    <td rowspan="2" style="vertical-align : middle;text-align:center;"><b>CHANGE CONFIGURATION FORM</b></td>
    <td><b>IT-0024</b></td>
  </tr>

  <tr class="center">
    <td><b>IT HQ</b></td>
  </tr>
</div>
</table>


<table border="3" style="width: 100%">
  <tr>
    <td>
 <table border="0 rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
    <label>Bussan Auto Finance
    </label>
  </td>
  </tr>
</div>
</table>

<table border="0" rules="none" style="width: 100%">
  <tr>
    <td>
 <table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
    <label>Date Request :
       <input type="text" value="<?=$date_request;?>"></input>
    </label>
  </td>
  </tr>
  <tr>
    <td>  
    <label>Department :
       <input type="text" value="<?=$department;?>"></input>
    </label>
  </td>
  </tr>
  <tr>
    <td>  
    <label>Purpose :
       <input type="text" value="<?=$purpose;?>"></input>
    </label>
  </td>
  </tr>
</div>
</table>

<table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
    <label>Equipments Affected :
    </label>
  </td>
  </tr>
</div>
</table>

 <table border="0" rules="none" class="table" style="width: 100%">
<tr>
  <td >
    <?php if($type1!='') {
      $check_type1 = 'checked';
    }
    ?>
   <input type="checkbox" size="5" <?=$check_type1;?>>
  </td>
  <td>
    <label>Type :
    </label>
   <input type="text" size="40" value="<?=$type1;?>"></input>
   <label>IP Address :
    </label>
    <input type="text" size="10" value="<?=$ip1;?>"></input>
  </td>
</tr>

<tr>
  <td >
     <?php if($type2!='') {
      $check_type2 = 'checked';
    }
    ?>
   <input type="checkbox" size="5" <?=$check_type2;?>>
  </td>
  <td>
    <label>Type :
    </label>
   <input type="text" size="40" value="<?=$type2;?>"></input>
   <label>IP Address :
    </label>
    <input type="text" size="10" value="<?=$ip2;?>"></input>
  </td>
</tr>

<tr>
  <td >
    <?php if($type3!='') {
      $check_type3 = 'checked';
    }
    ?>
   <input type="checkbox" size="5" <?=$check_type3;?>>
  </td>
  <td>
    <label>Type :
    </label>
   <input type="text" size="40" value="<?=$type3;?>"></input>
   <label>IP Address :
    </label>
    <input type="text" size="10" value="<?=$ip3;?>"></input>
  </td>
</tr>

<tr>
  <td >
      <?php if($type4!='') {
      $check_type4 = 'checked';
    }
    ?>
   <input type="checkbox" size="5" <?=$check_type4;?>>
  </td>
  <td>
    <label>Type :
    </label>
   <input type="text" size="40" value="<?=$type4;?>"></input>
   <label>IP Address :
    </label>
    <input type="text" size="10" value="<?=$ip4;?>"></input>
  </td>
</tr>
</table>

<table border="0" rules="none"  class="table text-left">
  <div>
  <tr>
    <td>  
      <label> Descriptions :
      </label>
    </td>
  </tr>
  <tr>
    <td>  
      <label> Configuration before Rollback :
      </label>
    </td>
  </tr>
    <tr>
    <td>
       <textarea class="form-control" rows="10"><?=$configuration_before_rollback;?></textarea>
     </td>
   </tr>

   <tr>
    <td>  
      <label> Configuration Plan of Rollback :
      </label>
    </td>
  </tr>
    <tr>
    <td>
       <textarea class="form-control" rows="10"><?=$configuration_plan_rollback;?></textarea>
     </td>
   </tr>
</div>

</table>

<div class="pull-right padding-right btn-space  margin-right  padding-right" >
<button type="submit" class="btn btn-primary" name="btn_approve" value="btn_approve">Aprroved</button>
<button type="submit" class="btn btn-danger" name="btn_reject" value="btn_reject">Reject</button>

</center>
 
</form>

</div>

</td>
</tr>

 <!-- tabel rollback -->

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
<script src="<?=base_url();?>assets/sweetalert/sweetalert.min.js"></script>
<script src="<?=base_url();?>assets/alert/js/qunit-1.18.0.js"></script>

<!-- <script type="text/javascript">
var url = '<?=base_url('Dashboard/tambah');?>';
   $('#ini-form').on('submit',function(e) {
    swal({
    title: "Simpan Data",
    text: "Apakah anda ingin menyimpan data ini ?",
    confirmButtonText:"Yakin",
    confirmButtonColor: "#002855",
    cancelButtonText:"Tidak",
    showCancelButton: true,
    closeOnConfirm: false,
    imageUrl: '<?=base_url('assets/images/imagessure.png');?>',
    showLoaderOnConfirm: true
    }, function () {
    $.ajax({
    url:url,
    data:$('#ini-form').serialize(),
    dataType:'text',
    type:'POST',
    success:function(e){    
if (e !== "gagal") {
  swal({
    title: "Success",
    confirmButtonColor: "#002855",
    text: "Data berhasil disimpan !.",
    imageUrl: '<?=base_url('assets/images/emotgood1.png');?>',
    },function(){
      window.location= '<?=base_url('dashboard/tambahdata');?>';
    });
    }
    else{
  swal({
    title: "Failed",
    confirmButtonColor: "#002855",
    text: "Data tidak berhasil disimpan !.",
    imageUrl: '<?=base_url('assets/images/emotsad.png');?>',
    });
    } 
    },
  error:function(xhr, ajaxOptions, thrownError){
  swal({
    title: "Failed",
    confirmButtonColor: "#002855",
    text: "Data tidak berhasil disimpan !.",
    imageUrl: '<?=base_url('assets/images/emotsad.png');?>',
  });
  }
  });
  return false;
  });
  e.preventDefault(); 
  });
</script> -->

<script type="text/javascript">
  $(document).ready(function(){
    $('input').prop('readonly', true);
    $('input[type="checkbox"]').prop('disabled', true);
    $('input[type="radio"]').prop('disabled', true);
    $('textarea').prop('readonly', true);
  })
</script>
 
</body>
</html>