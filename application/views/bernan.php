<!DOCTYPE html>
<html>
<head>
	<title>coba</title>

	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/DataTables/media/css/jquery.dataTables.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/sweetalert/sweetalert.css">

    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
  <li><a data-toggle="tab" href="#menu1">Menu 1</a></li>
</ul>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    
  	<!-- Table CCF -->
  		<!-- TABEL NO 1 -->
<table border="1" style="width: 100%">
  <tr>
    <td>
 <table border="0" rules="0" class="table text-right">
  <div>
  <tr>
    <td>  
    <label>No .
     <input type="text"></input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL CHANGE CATEGORY -->
    <table border="0" class="table" rules="none">
  <div>
  <tr>
    <td>
    <label>Change Category :
      <br> 
      <input type="checkbox" value="Permanent" name="checkbox"> Permanent </input> &nbsp;
      <input type="checkbox" value="Temporary" name="checkbox"> Temporary / Trial</input>
  </label>
    </label>
  </td>
    <td class="text-right">
    <label>Due Date :
     <input type="date" ></input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL TESTING -->
    <table border="0" class="table" rules="none">
  <div>
  <tr>
    <td>
    <label>Testing Required :
      <input type="checkbox" value="Permanent" name="checkbox"> Yes </input> &nbsp;
      <input type="checkbox" value="Temporary" name="checkbox"> No</input>
  </label>
    </label>
  </td>
    <td class="text-right">
    <label>Tested On :
      <input type="checkbox" value="Permanent" name="checkbox"> Production </input> &nbsp;
      <input type="checkbox" value="Temporary" name="checkbox"> Development</input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL NOTE  -->
<table border="0" rules="none" class="table">
  <div>
  <tr>
    <td>  
    <label>Note :
      <br>
     <textarea class="form-control" rows="5"  cols="200"></textarea>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL DATE OF REQUEST -->
<table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
    <label>Date Of Request :
     <input type="date" ></input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL AFFECTED -->
<table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
    <label>Affected Sector :
     <input type="text" style="width: 500px" ></input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL PURPOSE -->
<table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
      <label> Purpose Of Modification :
      </label>
    </td>
  </tr>
    <tr>
    <td>
       <textarea class="form-control" ></textarea>
     </td>
   </tr>
</div>
</table>

<!-- TABEL EXECUTION -->
<table border="0" rules="none" class="table text-left">
  <div>
  <tr>
    <td>  
    <label>Execution Date :
     <input type="text" ></input>
    </label>
  </td>
  </tr>
</div>
</table>

<!-- TABEL EQUIPMENTS --> 
      <label> Purpose Of Modification :
      </label>


    <table border="0" rules="none" class="table text-left" style="width: 100%">
      <tr>
  <td width="100px">
    <input type="checkbox" value="Permanent" name="checkbox" > Network Devices </input> &nbsp;
    <textarea class="form-control" rows="4" ></textarea>
  </td>
  <td width="100px">
  <input type="checkbox" value="Permanent" name="checkbox"> Serves </input> &nbsp;
  <textarea class="form-control" rows="4" ></textarea>
  </td>
  <td width="100px"><input type="checkbox" value="Permanent" name="checkbox"> Others </input> &nbsp;
  <textarea class="form-control" rows="4" ></textarea>
  </td>
</tr>
</table>

<!-- TABEL CHANGE -->
    <table border="0" rules="none" class="table text-left" style="width: 100%">
<tr>
  <td width="100px">
   <label> Detailed Change Description :
      </label>
    <textarea class="form-control" rows="10" ></textarea>
  </td>
  <td width="100px">
  <label> New Configuration :
      </label>
  <textarea class="form-control" rows="10" ></textarea>
  </td>
</tr>
</table>

<!-- TABEL DETAILED -->
<table border="0" rules="none" class="table text-left">
  <tr>
    <td>  
      <label> Detailed Change Impact :
      </label>
    </td>
  </tr>
  </table>
    <table border="0" rules="none" class="table text-left" style="width: 100%">
<tr>
  <td width="100px">
    <label> Business Impact :
      </label>
    <textarea class="form-control" rows="4" ></textarea>
  </td>
  <td width="100px">
     <label> Security Impact :
      </label>
  <textarea class="form-control" rows="4" ></textarea>
  </td>
</tr>
</table>

<!-- TABEL ATTACHMENT -->
<table border="0" rules="none"  class="table text-left">
  <div>
  <tr>
    <td>  
      <label> Attachment :
      </label>
    </td>
  </tr>
    <tr>
    <td>
       <textarea class="form-control" ></textarea>
     </td>
   </tr>
</div>
</table>


</td>
</tr>

</div>
</div>

 </table>
  	<!-- Table CCF -->

  </div>
  <div id="menu1" class="tab-pane fade">

    <table>
    	<tr>
    		<td>1</td>
    	</tr>
    </table>

  </div>
</div>

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
	<script src="<?=base_url();?>assets/sweetalert/sweetalert.min.js"></script>
	<script src="<?=base_url();?>assets/alert/js/qunit-1.18.0.js"></script>

	</body>
</html>