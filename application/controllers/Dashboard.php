<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
    }

    public function index()
    {
        if($this->admin->logged_id())
        {
            
            $this->load->view("dashboard");         

        }else{

            //jika session belum terdaftar, maka redirect ke halaman login
            redirect("login");

        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

    public function approve(){
        $btn_approve = $this->input->post('btn_approve');
        $btn_reject = $this->input->post('btn_reject');
        $no_ccf = $this->input->post('no_ccf');

        if($btn_approve){
            $is_approve = '1';
        } 

        if($btn_reject){
            $is_approve = '2';
        } 

        $data = array(
            'no_ccf' => $no_ccf,
            'is_approve' => $is_approve
        );

        $update = $this->admin->update_approve($data);

        if($update){
             echo "<script type='text/javascript'>alert('Berhasil');window.location='data_ccf';</script>";
        //     echo "<script type='text/javascript'>alert('Berhasil');
        //     window.location.href='data_ccf';
        // </script>";
        } else {
        //     echo "<script type='text/javascript'>alert('GAGALLLLL');
        //     window.location.href='data_ccf';
        // </script>";
            echo "<script type='text/javascript'>alert('ggggg');window.location='data_ccf';</script>";
        }

    }

    public function tambahdata(){
        
        $level = $this->session->userdata('level');

        $no_ccf = $this->input->get('no_ccf');

        
        
        if($level=='admin' AND $no_ccf!=''){
            $data = $this->admin->getDataByNoCCF($no_ccf);

        foreach ($data as $key => $value) {
            $data['no_ccf'] = $value['no_ccf'];
            $data['due_date'] = $value['due_date'];
            $data['note'] = $value['note'];
            $data['date_of_request'] = $value['date_of_request'];
            $data['affected_sector'] = $value['affected_sector'];
            $data['purpose_of_modification'] = $value['purpose_of_modification'];
            $data['execution_date'] = $value['execution_date'];
            $data['detailed_change_description'] = $value['detailed_change_description'];
            $data['new_configuration'] = $value['new_configuration'];
            $data['business_impact'] = $value['business_impact'];
            $data['security_impact'] = $value['security_impact'];
            $data['date_request'] = $value['date_request'];
            $data['department'] = $value['department'];
            $data['purpose'] = $value['purpose'];
            $data['configuration_before_rollback'] = $value['configuration_before_rollback'];
            $data['configuration_plan_rollback'] = $value['configuration_plan_rollback'];
            $data['change_category'] = $value['change_category'];
            $data['testing_required'] = $value['testing_required'];
            $data['attachment'] = $value['attachment'];




            $equipment_affected = json_decode($value['equipment_affected']);
            $tested_on = $value['tested_on'];
            $equipment_affected_rollback = json_decode($value['equipment_affected_rollback']);
        }

        //echo $equipment_affected_rollback[3]->ip;
        //echo '<pre>';print_r($equipment_affected_rollback);die;
        $data['network_devices'] = $equipment_affected->network_devices;
        //echo $data['network_devices'];die;
        $data['servers'] = $equipment_affected->servers;
        $data['others'] = $equipment_affected->others;


        $data['type1'] = $equipment_affected_rollback[0]->type;
        $data['ip1'] = $equipment_affected_rollback[0]->ip;
        $data['type2'] = $equipment_affected_rollback[1]->type;
        $data['ip2'] = $equipment_affected_rollback[1]->ip;
        $data['type3'] = $equipment_affected_rollback[2]->type;
        $data['ip3'] = $equipment_affected_rollback[2]->ip;
        $data['type4'] = $equipment_affected_rollback[3]->type;
        $data['ip4'] = $equipment_affected_rollback[3]->ip;

        // ini untuk menampilkan data tested on
        if (strpos($tested_on, 'Development') !== false) {
            $development = 'checked';
        } 

        if (strpos($tested_on, 'Production') !== false) {
            $production = 'checked';
        } 

        $data['development'] = $development;
        $data['production'] = $production;

          $this->load->view("tambahdata_admin", $data); 
        } elseif($level=='admin' AND $no_ccf==''){
          $this->load->view("tambahdata_admin"); 
        }  else {
           $this->load->view("tambahdata_user");
        }

         
    }

     public function dashboard2(){
        $this->load->view("dashboard2");  
    }

      public function data_ccf(){
        $data['ccf'] = $this->admin->getData();
        $this->load->view("data_ccf", $data);  
    }

   
    function tambah()
    {
        $data=array(
        'nim'=>$this->input->post('nim'),
        'nama'=>$this->input->post('nama'),
        'jurusan'=>$this->input->post('jurusan'),
        'alamat'=>$this->input->post('alamat')
        );

        $coba = $this->admin->tambah($data);
        //echo $coba;
        //print_r($coba);
        
        if($coba){
            echo "<script type='text/javascript'>alert('$coba');</script>";
        } else {
            echo 'Gagal';
        }
    }

    public function coba(){
        $equipment_affected = array('network-devices' => $this->input->post('network-devices'),
                                    'servers' => $this->input->post('servers'),
                                    'others' => $this->input->post('others'),
                                );
        echo '<pre>';print_r(json_encode($equipment_affected));
    }

    public function ayeaye(){
        $data = $this->Coba_M->getData();

		
		foreach ($data as $key => $value) {
			$no_ccf = $value['no_ccf'];
			$equipment_affected = json_decode($value['equipment_affected']);
			$tested_on = $value['tested_on'];
			$equipment_affected_rollback = json_decode($value['equipment_affected_rollback']);
		}

		
		// ini untuk menampilkan data equipment affected rollback
		foreach ($equipment_affected_rollback as $key => $value1) {
			echo $value1->ip_address;
		}
		// ini untuk menampilkan data equipment affected rollback


		// ini untuk menampilkan data tested on
		if (strpos($tested_on, 'Development') !== false) {
		    $development = 'true';
		} else {
			$development = 'false';
		}

		if (strpos($tested_on, 'Production') !== false) {
		    $production = 'true';
		} else {
			$production = 'false';
		}
    }

    public function insert () {

        $type = $this->input->post('equipment_affected_rollback_type');
        $ip = $this->input->post('equipment_affected_rollback_ip');

       $equipment_affected_rollback = array(
                                 '0' => array('type' => $type[0],
                                            'ip' => $ip[0]
                                ),
                                 '1' => array('type' => $type[1],
                                            'ip' => $ip[1]
                                ),
                                 '2' => array('type' => $type[2],
                                            'ip' => $ip[2]
                                ),
                                 '3' => array('type' => $type[3],
                                            'ip' => $ip[3]
                                )
                       );
       $equipment_affected_rollback = json_encode($equipment_affected_rollback);

        $equipment_affected = array('network_devices' => $this->input->post('network-devices'),
                                    'servers' => $this->input->post('servers'),
                                    'others' => $this->input->post('others'),
                                );

        $equipment_affected = json_encode($equipment_affected);

        $tested_on = $this->input->post('tested_on_production').';'.$this->input->post('tested_on_development');

        

    $config['upload_path']          = './uploads/';
    $config['allowed_types']        = 'gif|jpg|png|xlsx|docx|xls|doc';
    $config['max_size']             = 1000;
    $config['max_width']            = 1024;
    $config['max_height']           = 768;

    $this->load->library('upload', $config);

    if ( ! $this->upload->do_upload('attachment'))
    {
            $error = array('error' => $this->upload->display_errors());

            //$this->load->view('upload_form', $error);
    }
    else
    {
            $data = array('upload_data' => $this->upload->data());
            $file_name = $data['upload_data']['file_name'];
            //echo '<pre>';print_r($data);
            //echo $file_name;die;
            

            //$this->load->view('upload_success', $data);
    }
    
    $data = array(
        'no_ccf' => $this->input->post('no'),
        'change_category' => $this->input->post('change-category'),
        'due_date' => $this->input->post('due-date'),
        'testing_required' => $this->input->post('testing_required'),
        'tested_on' => $tested_on,
        'note' => $this->input->post('note'),
        'date_of_request' => $this->input->post('date-of-request'),
        'affected_sector' => $this->input->post('affected-sector'),
        'purpose_of_modification' => $this->input->post('purpose-of-modification'),
        'execution_date' => $this->input->post('execution-date'),
        'equipment_affected' => $equipment_affected,
        'detailed_change_description' =>  $this->input->post('detailed-change-des'),
        'new_configuration' => $this->input->post('new-configuration'),
        'business_impact' => $this->input->post('business-impact'),
        'security_impact' => $this->input->post('security-impact'),
        'attachment' => $file_name,
        'date_request' => $this->input->post('date-request'),
        'department' => $this->input->post('department'),
        'purpose' => $this->input->post('purpose'),
        'equipment_affected_rollback' => $equipment_affected_rollback,
        'configuration_before_rollback' => $this->input->post('configuration-before-rollback'),
        'configuration_plan_rollback' => $this->input->post('configuration-plan-rollback'),
        'is_approve' => '0',

    );

        $insert = $this->admin->insert($data);
          if($data){
            echo "<script type='text/javascript'>alert('Berhasil');window.location='ccf';</script>";
        } else {
            echo "<script type='text/javascript'>alert('Gagal');window.location='ccf';</script>";
        }

        

}
     public function ccf(){
        $data['ccf'] = $this->admin->getData();
        $this->load->view("ccf", $data);  
    }

 
}
